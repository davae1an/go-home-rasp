package main

import (
	"fmt"
	"log"
	"runtime"
	"time"

	gosocketio "github.com/graarh/golang-socketio"
	"github.com/graarh/golang-socketio/transport"
	"gobot.io/x/gobot/drivers/gpio"
	"gobot.io/x/gobot/platforms/raspi"
)

// Channel the channel to join
type Channel struct {
	Channel string `json:"channel"`
}

// RelayMessage : the relay message object to be parsed
type RelayMessage struct {
	Channel int    `json:"channel"`
	State   string `json:"state"`
	JWT     string `json:"jwt,omitempty"`
}

func sendJoin(c *gosocketio.Client) {
	log.Println("Acking /join")
	result, err := c.Ack("/join", Channel{"main"}, time.Second*5)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Println("Ack result to /join: ", result)
	}
}

//GOARM=6 GOARCH=arm GOOS=linux go build testy.go

func connect() *gosocketio.Client {

	for {

		c, err := gosocketio.Dial(
			gosocketio.GetUrl("go.aship.in", 443, true),
			transport.GetDefaultWebsocketTransport())
		if err == nil {
			log.Println("Connected to Server")
			return c
		}

		log.Println(err)
		fmt.Println("reconnecting in 10sec")

		time.Sleep(10 * time.Second)
	}

}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	Ch1 := "off"
	Ch2 := "off"
	Ch3 := "off"
	Ch4 := "off"
	r := raspi.NewAdaptor()
	RelayCH1 := gpio.NewRelayDriver(r, "35") // BCM 19
	RelayCH2 := gpio.NewRelayDriver(r, "37") //.BCM 26
	RelayCH3 := gpio.NewRelayDriver(r, "38") // BCM 20
	RelayCH4 := gpio.NewRelayDriver(r, "40") // BCM 21

	c := connect()
	go sendJoin(c)

	for {

		c.On(gosocketio.OnDisconnection, func(h *gosocketio.Channel) {
			log.Println("Disconnected Attepting to Reconnect")
			c = connect()
		})

		c.On("relay_status", func(h *gosocketio.Channel, arg string) {

			switch arg {
			case "ch1":
				h.Emit("relay_res", RelayMessage{1, Ch1, ""})
			case "ch2":
				h.Emit("relay_res", RelayMessage{2, Ch2, ""})
			case "ch3":
				h.Emit("relay_res", RelayMessage{3, Ch3, ""})
			case "ch4":
				h.Emit("relay_res", RelayMessage{4, Ch4, ""})
			}

		})

		c.On("relay_res", func(h *gosocketio.Channel, args RelayMessage) {
			log.Println("--- Relay Res: ", args)
		})

		c.On("relay_trig", func(h *gosocketio.Channel, args RelayMessage) {
			switch args.Channel {
			case 1:
				if args.State == "on" {
					RelayCH1.Off()
					h.Emit("relay_res", RelayMessage{1, "on", ""})
					Ch1 = "on"
					fmt.Println("one is on")
				} else {
					RelayCH1.On()
					h.Emit("relay_res", RelayMessage{1, "off", ""})
					Ch1 = "off"
					fmt.Println("one is off")
					h.BroadcastTo("main", "relay_res", RelayMessage{1, "off", ""})
				}

			case 2:
				if args.State == "on" {
					h.Emit("relay_res", RelayMessage{2, "on", ""})
					Ch2 = "on"
					RelayCH2.Off()
					fmt.Println("two is on")
				} else {
					h.Emit("relay_res", RelayMessage{2, "off", ""})
					Ch2 = "off"
					RelayCH2.On()
					fmt.Println("two is off")
				}

			case 3:
				if args.State == "on" {
					h.Emit("relay_res", RelayMessage{3, "on", ""})
					Ch3 = "on"
					RelayCH3.Off()
					fmt.Println("three is on")
				} else {
					h.Emit("relay_res", RelayMessage{3, "off", ""})
					Ch3 = "off"
					RelayCH3.On()
					fmt.Println("three is off")
				}

			case 4:
				if args.State == "on" {
					h.Emit("relay_res", RelayMessage{4, "on", ""})
					Ch4 = "on"
					RelayCH4.Off()
					fmt.Println("four is on")
				} else {
					h.Emit("relay_res", RelayMessage{4, "off", ""})
					Ch4 = "off"
					RelayCH4.On()
					fmt.Println("four is off")
				}
			}
		})

		// reader := bufio.NewReader(os.Stdin)

		// data, _, _ := reader.ReadLine()

		// command := string(data)
		// // c.Emit("relay", RelayMessage{1, command})
		// log.Printf("send message:%v\n", command)
	}

}
