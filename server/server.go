package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/graarh/golang-socketio"
	"github.com/graarh/golang-socketio/transport"
)

// Channel the channel to join
type Channel struct {
	Channel string `json:"channel"`
}

// RelayMessage : the relay message object to be parsed
type RelayMessage struct {
	Channel int    `json:"channel"`
	State   string `json:"state"`
	JWT     string `json:"jwt,omitempty"`
}

func main() {

	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "3811"
	}

	server := gosocketio.NewServer(transport.GetDefaultWebsocketTransport())

	server.On(gosocketio.OnConnection, func(c *gosocketio.Channel) {
		log.Println("Connected")
		c.Join("main")
	})
	server.On(gosocketio.OnDisconnection, func(c *gosocketio.Channel) {
		log.Println("Disconnected")
	})

	server.On("/join", func(c *gosocketio.Channel, channel Channel) string {
		time.Sleep(2 * time.Second)
		log.Println("Client joined to ", channel.Channel)
		return "joined to " + channel.Channel
	})

	// Send data here to trigger relays
	server.On("relay", func(c *gosocketio.Channel, args RelayMessage) {
		fmt.Println(args)

		//Check for JWT

		//JWT valid send message
		c.BroadcastTo("main", "relay_trig", RelayMessage{args.Channel, args.State, ""})
	})

	// Implement this on client side to update app state
	server.On("relay_res", func(c *gosocketio.Channel, args RelayMessage) {
		fmt.Println("received relay: ", args)
		c.BroadcastTo("main", "relay_res", RelayMessage{args.Channel, args.State, ""})
	})

	server.On("relay_status", func(c *gosocketio.Channel, arg string) {
		c.BroadcastTo("main", "relay_status", arg)
	})

	serveMux := http.NewServeMux()
	serveMux.Handle("/socket.io/", server)

	serveMux.HandleFunc("/", hello)
	serveMux.Handle("/favicon.ico", http.NotFoundHandler())

	// print env
	env := os.Getenv("APP_ENV")
	if env == "production" {
		log.Println("Running Go-Home server in production mode on " + PORT)
	} else {
		log.Println("Running Go-Home server in dev mode on " + PORT)
	}
	// log.Panic()
	http.ListenAndServe(":"+PORT, serveMux)

}

func hello(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Go-Home Server v1")
}
