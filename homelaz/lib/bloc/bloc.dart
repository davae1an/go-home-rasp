import 'dart:async';
import 'package:rxdart/rxdart.dart';

class Bloc {
  final _switch1 = BehaviorSubject<bool>();
  final _switch2 = BehaviorSubject<bool>();
  final _switch3 = BehaviorSubject<bool>();

  Stream<bool> get switch1 => _switch1.stream;
  Stream<bool> get switch2 => _switch2.stream;
  Stream<bool> get switch3 => _switch3.stream;

  Function(bool) get changeSwitch1 => _switch1.sink.add;
  Function(bool) get changeSwitch2 => _switch2.sink.add;
  Function(bool) get changeSwitch3 => _switch3.sink.add;

  dispose() {
    _switch1.close();
    _switch2.close();
    _switch3.close();
  }

  turnSwitch1() {}

  turnSwitch2() {}

  turnSwitch3() {}
}
