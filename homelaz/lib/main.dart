import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Lazy Hub';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: ListView(
          children: <Widget>[
            ListItem(name: "Kitchen"),
            ListItem(name: "Room"),
            ListItem(
              name: "Outside",
            )
          ],
        ),
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  final String name;
  ListItem({this.name});
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                CircleAvatar(
                  child: new Text(name[0]),
                ),
                Padding(padding: EdgeInsets.only(right: 10.0)),
                Text(
                  name,
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
            CupertinoSwitch(
              value: true,
              onChanged: (bool value) {
                print("done");
              },
            ),
          ],
        ),
      ),
    );
  }
}
