import 'package:socket_io_client/socket_io_client.dart' as IO;

class Sock {
  final String url;
  IO.Socket socket;

  Sock({this.url}) {
    print(this.url);

    socket = IO.io(this.url);
    socket.on('connect', (_) {
      print('connected to server');
      tester();
      socket.emit('msg', 'test');
    });
    socket.on('event', (data) => print(data));
    socket.on('disconnect', (_) => print('disconnect'));
    socket.on('fromServer', (_) => print(_));
  }

  tester() {
    print('called function from cinstructor');
  }
}

final sok = Sock(url: "https://go.aship.in");
